# pesky

read-only interface for skype db, message browsing (probably only old versions)

## Quick start

```sh
gleam run   # Run the project
gleam test  # Run the tests
```

expects sqlite database in same directory as application runs from. can setup from `src/pesky.sql`

