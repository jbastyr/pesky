import gleam/erlang/process
import mist
import pesky/web

pub fn main() {
  web.router
  |> mist.new
  |> mist.port(4000)
  |> mist.start_http

  process.sleep_forever()
}

