import gleam/http/response.{Response}
import gleam/list
import gleam/option.{None}
import gleam/result
import gleam/string
import mist.{ResponseData}
import pesky/common.{not_found}

pub fn serve(parts: List(String)) -> Response(ResponseData) {
  let file_path = string.join(parts, "/")


  mist.send_file(file_path, offset: 0, limit: None)
  |> result.map(fn(file) {
    let content_type = guess_content_type(file_path)
    response.new(200)
    |> response.prepend_header("content-type", content_type)
    |> response.set_body(file)
  })
  |> result.lazy_unwrap(not_found)
}

fn guess_content_type(file_path: String) -> String {
  let assert Ok(file) = string.split(file_path, "/") |> list.last
  let assert Ok(extension) = string.split(file, ".") |> list.last

  case extension {
    "html" -> "text/html"
    "css" -> "text/css"
    "js" -> "application/javascript"
    _ -> "application/octet-stream"
  }
}
