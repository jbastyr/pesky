import gleam/dynamic.{element}
import gleam/http
import gleam/http/request.{Request}
import gleam/http/response.{Response}
import mist.{Connection, ResponseData}
import pesky/common.{not_found, resp, get_query_opt}
import pesky/known_db.{must_find}
import gleam/json.{object, array, Json}
import gleam/option.{None, Some}
import sqlight

pub type Conversation {
  Conversation(id: Int, name: String, last_active: Int, message_count: Int)
}

fn json_encoder(conv: Conversation) -> Json {
  object([
    #("id", json.int(conv.id)),
    #("identity", json.string(conv.name)),
    #("name", json.int(conv.last_active)),
    #("message_count", json.int(conv.message_count))
  ])
}

fn entity_decoder() -> dynamic.Decoder(Conversation) {
  dynamic.decode4(
    Conversation,
    element(0, dynamic.int),
    element(1, dynamic.string),
    element(2, dynamic.int),
    element(3, dynamic.int)
  )
}

pub fn routes(db_id: Int, parts: List(String), req: Request(Connection)) -> Response(ResponseData) {
  case req.method, parts, req.query {
    http.Get, [], None -> list_(db_id)
    http.Get, [], Some(_) -> handle_query(db_id, req)
    _, _ , _ -> not_found()
  }
}

fn handle_query(db_id: Int, req: Request(Connection)) -> Response(ResponseData) {
  case get_query_opt(req, "q") {
    Ok(filter) -> list_with_filter_(db_id, filter)
    Error(Nil) -> list_(db_id)
  }
}


fn list_(db_id: Int) -> Response(ResponseData) {
  let src = must_find(db_id)
  use db <- sqlight.with_connection("file:" <> src.path)
  let conv = list(db)
  let json = array(from: conv, of: json_encoder)

  resp(200, json)
}

fn list_with_filter_(db_id: Int, filter: String) -> Response(ResponseData) {
  let src = must_find(db_id)
  use db <- sqlight.with_connection("file:" <> src.path)
  let conv = list_with_filter(db, filter)
  let json = array(from: conv, of: json_encoder)

  resp(200, json)
}

fn list(db: sqlight.Connection) -> List(Conversation) {
  let sql =
"
select 
    c.id,
    c.displayname,
    case when c.last_activity_timestamp is null then 0 else c.last_activity_timestamp end,
    count(*)
from conversations c
join messages m
    on c.id = m.convo_id
group by c.id
having count(*) > 0
order by last_activity_timestamp desc
"
  let assert Ok(rows) = sqlight.query(
    sql,
    on: db,
    with: [],
    expecting: entity_decoder()
  )

  rows
}

fn list_with_filter(db: sqlight.Connection, filter: String) -> List(Conversation) {
  let sql = 
"
select
  c.id,
  c.displayname,
  case when c.last_activity_timestamp is null then 0 else c.last_activity_timestamp end,
  count(*)
from conversations c
join messages m
  on c.id = m.convo_id
where m.body_xml like '%'|| $1 ||'%'
group by c.id
having count(*) > 0
order by last_activity_timestamp desc
"
  let assert Ok(rows) = sqlight.query(
    sql,
    on: db,
    with: [sqlight.text(filter)],
    expecting: entity_decoder()
  )

  rows
}

