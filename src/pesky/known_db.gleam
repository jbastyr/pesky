import gleam/bit_builder
import gleam/dynamic.{field, element}
import gleam/http/request.{Request}
import gleam/http/response.{Response}
import gleam/http
import gleam/int
import gleam/list
import gleam/json.{object, array, Json}
import mist.{Connection, ResponseData}
import pesky/common.{not_found, resp}
import sqlight

pub type KnownDatabase {
  KnownDatabase(id: Int, name: String, path: String)
}

pub type KnownDatabaseNew {
  KnownDatabaseNew(name: String, path: String)
}

fn json_decoder() -> dynamic.Decoder(KnownDatabaseNew) {
  dynamic.decode2(
    KnownDatabaseNew,
    field("name", of: dynamic.string),
    field("path", of: dynamic.string)
  )
}

fn json_encoder(db: KnownDatabase) -> Json {
  object([
    #("id", json.int(db.id)),
    #("name", json.string(db.name)),
    #("path", json.string(db.path))
  ])
}

fn entity_decoder() -> dynamic.Decoder(KnownDatabase) {
  dynamic.decode3(
    KnownDatabase,
    element(0, dynamic.int),
    element(1, dynamic.string),
    element(2, dynamic.string)
  )
}


pub fn routes(parts: List(String), req: Request(Connection)) -> Response(ResponseData) {
  case req.method, parts {
    http.Get, [] -> list_()
    http.Post, [] -> add_(req)
    http.Delete, [id] -> delete_(id)
    _, _ -> not_found()
  }
}

pub fn must_find(db_id: Int) -> KnownDatabase {
  use db <- sqlight.with_connection("file:pesky.db")
  let assert Ok(found) = find(db, db_id)
  found
}

fn list_() -> Response(ResponseData) {
  use db <- sqlight.with_connection("file:pesky.db")
  let known_dbs = list(db)
  let json = array(from: known_dbs, of: json_encoder)
  
  resp(200, json)
}

fn add_(req: Request(Connection)) -> Response(ResponseData) {
  let assert Ok(body) = mist.read_body(req, 1024)
  let assert Ok(item) = json.decode_bits(from: body.body, using: json_decoder())

  use db <- sqlight.with_connection("file:pesky.db")
  let created = add(item, db)
  let json = json_encoder(created)

  resp(201, json)
}

fn delete_(id: String) -> Response(ResponseData) {
  let assert Ok(id) = int.parse(id)

  use db <- sqlight.with_connection("file:pesky.db")
  delete(id, db)

  response.new(200)
  |> response.set_body(mist.Bytes(bit_builder.new()))
}

fn find(db: sqlight.Connection, id: Int) -> Result(KnownDatabase, Nil) {
  let sql = 
"
select id, name, path
from known_dbs
where id = $1
"
  
  let assert Ok(rows) = sqlight.query(
    sql,
    on: db,
    with: [sqlight.int(id)],
    expecting: entity_decoder()
  )


  list.first(rows)
}

fn list(db: sqlight.Connection) -> List(KnownDatabase) {
  let sql = 
"
select id, name, path
from known_dbs
order by id asc;
"

  let assert Ok(rows) = sqlight.query(
    sql,
    on: db,
    with: [],
    expecting: entity_decoder()
  )

  rows
}

fn add(item: KnownDatabaseNew, db: sqlight.Connection) -> KnownDatabase {
  let sql =
"
insert into known_dbs(name, path)
values ($1, $2)
returning id, name, path;
"

  let assert Ok(rows) = sqlight.query(
    sql,
    on: db,
    with: [sqlight.text(item.name), sqlight.text(item.path)],
    expecting: entity_decoder()
  )

  let assert [row] = rows

  row
}

fn delete(id: Int, db: sqlight.Connection) -> Nil {
  let sql =
"
delete from known_dbs
where id = $1
returning id;
"
  let assert Ok(_) = sqlight.query(
    sql,
    on: db,
    with: [sqlight.int(id)],
    expecting: Ok
  )

  Nil
}

