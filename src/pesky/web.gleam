import gleam/http/request.{Request}
import gleam/http/response.{Response}
import mist.{Connection, ResponseData}
import pesky/known_db
import pesky/static
import pesky/conversation
import pesky/message
import pesky/common.{not_found, must_parse}

pub fn router(req: Request(Connection)) -> Response(ResponseData) {
  let segments = request.path_segments(req)
  case segments {
    ["favicon.ico"] -> static.serve(["static", "favicon.ico"])
    ["static"] -> static.serve(["static", "index.html"])
    ["static", _] -> static.serve(segments)
    ["known", ..rest] -> known_db.routes(rest, req)
    ["v1", id, "conv", ..rest] -> conversation.routes(must_parse(id), rest, req)
    ["v1", id, "msg", ..rest] -> message.routes(must_parse(id), rest, req)
    _ -> not_found()
  }
}

