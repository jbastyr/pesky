import gleam/dynamic.{element}
import gleam/http
import gleam/http/request.{Request}
import gleam/http/response.{Response}
import mist.{Connection, ResponseData}
import pesky/common.{not_found, resp, must_parse, html_decode}
import pesky/known_db.{must_find}
import gleam/json.{object, array, Json}
import sqlight

pub type Message {
  Message(id: Int, author: String, msg_type: String, body: String)
}

fn json_encoder(msg: Message) -> Json {
  object([
    #("id", json.int(msg.id)),
    #("author", json.string(msg.author)),
    #("type", json.string(msg.msg_type)),
    #("body", json.string(html_decode(msg.body)))
  ])
}

fn entity_decoder() -> dynamic.Decoder(Message) {
  dynamic.decode4(
    Message,
    element(0, dynamic.int),
    element(1, dynamic.string),
    element(2, dynamic.string),
    element(3, dynamic.string)
  )
}

pub fn routes(db_id: Int, parts: List(String), req: Request(Connection)) -> Response(ResponseData) {
  case req.method, parts {
    http.Get, [convo_id] -> list_(db_id, must_parse(convo_id))
    _, _ -> not_found()
  }
}

fn list_(db_id: Int, convo_id: Int) -> Response(ResponseData) {
  let src = must_find(db_id)
  use db <- sqlight.with_connection("file:" <> src.path)
  let msgs = list(db, convo_id)
  let json = array(from: msgs, of: json_encoder)

  resp(200, json)
}

fn list(db: sqlight.Connection, convo_id: Int) -> List(Message) {
  let sql = 
"
select
    id,
    author,
    case
      when type = 60 then '/me'
      when type = 61 then 'msg'
      when type = 68 then 'file'
      else 'other'
    end as type,
    body_xml
from messages
where convo_id = $1
    and type in (60,61,68)
order by id desc
"

  let assert Ok(rows) = sqlight.query(
    sql,
    on: db,
    with: [sqlight.int(convo_id)],
    expecting: entity_decoder()
  )

  rows
}
