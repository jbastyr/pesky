import gleam/bit_builder
import gleam/http/request.{Request}
import gleam/http/response.{Response}
import gleam/int
import gleam/list
import gleam/string
import mist.{ResponseData, Connection}
import gleam/json.{Json}
import gleam/result
import gleam/map

pub fn not_found() {
  response.new(404)
  |> response.set_body(mist.Bytes(bit_builder.new()))
}

fn to_response(json: Json) -> ResponseData {
  json
  |> json.to_string_builder
  |> bit_builder.from_string_builder
  |> mist.Bytes
}

pub fn resp(status: Int, json: Json) -> Response(ResponseData) {
  response.new(status)
  |> response.set_header("content-type", "application/json")
  |> response.set_body(to_response(json))
}

pub fn must_parse(id: String) -> Int {
  let assert Ok(ret) = int.parse(id)
  ret
}

pub fn html_decode(text: String) -> String {
  let subs = [
    #("&quot;", "\""),
    #("&apos;", "'"),
    #("&gt;", ">"),
    #("&lt;", "<"),
    #("&amp;", "&"),
  ]

  let decoded = list.fold(subs, text, fn(acc, sub) {
    string.replace(acc, each: sub.0, with: sub.1)
  })
  
  decoded
}

pub fn get_query_opt(req: Request(Connection), opt: String) -> Result(String, Nil) {
  request.get_query(req)
  |> result.unwrap([])
  |> map.from_list
  |> map.get(opt)
}

